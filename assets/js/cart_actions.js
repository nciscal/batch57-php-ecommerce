
//=================================================
// Initialize Cart Page
//=================================================

$(document).ready(function(){

	currentPage = window.location.href

	//=================================================
	// Initialize PAYPAL
	//=================================================

	if (currentPage.includes("cart.php")) {
	  
		paypal.Buttons({
			onClick: function() {
				if ($('#cart-count').html() == 0) {
		        	Swal.fire(
		        		'Sorry',
		        		'Your cart is empty',
		        		'error'
		        		);
		        	return false;
		        }
			},
		    createOrder: function(data, actions) {

		        // simple regex to remove unnecessary strings
		        totalAmount = document.getElementById('total-amount').innerHTML.replace(/[^0-9.]/g,"");

		        //Prevent Submitting when cart is empty
		        
		        // This function sets up the details of the transaction, including the amount and line item details.
		        return actions.order.create({
		        	purchase_units: [{
		        		amount: {
		        			value: totalAmount
		        		}
		        	}]
		        });
		    },
		    onApprove: function(data, actions) {
		        // This function captures the funds from the transaction.
		        return actions.order.capture().then(function(details) {

		        	formData = new FormData();
		        	formData.append("orderID",data.orderID);
		        	option = {
		        		method: 'POST',
		        		body: formData
		        	}

		        	fetch("http://localhost:8000/controllers/create_paypal_transaction.php", option)
		        	.then ((response)=> {
		        		return response.text();
		        	})
		        	.then((data) => {
		        		emptyCart();

		        		if (data.replace(/\s/g, "") == "success") {
		        			Swal.fire({
		        				title: 'Payment Succesful',
		        				text: `Thank you for shopping with PushCart`,
		        				icon: 'success',
		        				confirmButtonText: 'OK',
		        				confirmButtonColor: '#3085d6'
		        			}).then((result) => {
		        				if (result.value) {
		        					window.location.href = "./../../views/transactions.php"
		        				}
		        			})
		        		} else {
		        			Swal.fire(
		        				'Sorry',
		        				'A Problem Occured',
		        				'error'
		        			);
		        		}
		        	})
		        });
		    }
		}).render('#paypal-button-container');

	};

	//=================================================
	// Bind click function to body
	// to deselect nodes in tree
	//=================================================

	if (currentPage.includes("transactions.php")) {

		$('body').click(function (e) {

		    if(e.target.id == "transactionTree")
		        return;
		      
		    if($(e.target).closest('#transactionTree').length)
		        return;  

		    $("#transactionTree").jstree().deselect_all(true);
		})
	};

	//=================================================
	// Initialize edit quantity buttons
	//=================================================

	$(".qtyinput").each (function(){
			if ($(this).val() <= 1) {
				$(this).prev().children('.cart-minus').prop('disabled', true);
				$(this).prev().children('.cart-minus').addClass('disabled');
			} else {
				$(this).prev().children('.cart-minus').prop('disabled', false);
				$(this).prev().children('.cart-minus').removeClass('disabled');
			};

			if ($(this).val() >= 100) {
				$(this).next().children('.cart-add').prop('disabled', true);
				$(this).next().children('.cart-add').addClass('disabled');
			} else {
				$(this).next().children('.cart-add').prop('disabled', false);
				$(this).next().children('.cart-add').removeClass('disabled');
			};
	})

});

//=================================================
// Quantity Buttons Events
//=================================================

let editQtyId;
let newQty;
let editData;

$('.cart-minus').click(function(e) {
	
	if ($(this).parent().next().val() == 1) {
		$(this).prop('disabled', true);
		$(this).addClass('disabled');
		$(this).parent().next().trigger('change');
	} else {
		$(this).prop('disabled', false);
		$(this).removeClass('disabled');
		$(this).parent().next().val(function (i, value) {
			return --value;
		});
		$(this).parent().next().trigger('change');
	}
})

$('.cart-add').click(function(e) {
	
	if ($(this).parent().prev().val() == 100) {
		$(this).prop('disabled', true);
		$(this).addClass('disabled');
		$(this).parent().prev().trigger('change');
	} else {
		$(this).prop('disabled', false);
		$(this).removeClass('disabled');
		$(this).parent().prev().val( function (i, value) {
			return ++value;
		});
		$(this).parent().prev().trigger('change');
	}
})

$(".edit-cart-qty-form").each(()=> {
	$(this).submit((e)=> {
		e.preventDefault();
	})
})

$(".qtyinput").blur(function(e) { 
      	$(this).trigger('change');
    }
);
$(".qtyinput").change(function(e) {

    	if ($(this).val() <= 1) {
    		$(this).prev().children('.cart-minus').prop('disabled', true);
    		$(this).prev().children('.cart-minus').addClass('disabled');
    	} else {
    		$(this).prev().children('.cart-minus').prop('disabled', false);
    		$(this).prev().children('.cart-minus').removeClass('disabled');
    	};

    	if ($(this).val() >= 100) {
    		$(this).next().children('.cart-add').prop('disabled', true);
    		$(this).next().children('.cart-add').addClass('disabled');
    	} else {
    		$(this).next().children('.cart-add').prop('disabled', false);
    		$(this).next().children('.cart-add').removeClass('disabled');
    	};
    	editQtyId = $(this).siblings('.product-id').val();
    	newQty = $(this).val();
    	editData = {'id': editQtyId, 'quantity': newQty, 'action': 'edit_quantity'}
    	elSubprice = '#subprice'+editQtyId;
    	editQty();
});

//=================================================
// Checkout Button Event
//=================================================

$('#checkOutBtn').click(()=>{$('#payment-mode-form').submit()})

$("#payment-mode-form").submit(function(e) {
	e.preventDefault();

	checkOutForm = $("#payment-mode-form")
	checkOutData = new FormData(this);

	if ($('#cart-count').html() == 0) {
		Swal.fire(
            'Sorry',
            'Your cart is empty',
            'error'
        );
        return;
	}
	$.ajax({
		url: './../../controllers/create_transactions.php',
		type: 'POST',
		data: checkOutData,
		processData: false,
		contentType: false,
		success: function(response) {
			if (response.replace(/\s/g, "") == 'success') {
				Swal.fire({
					title: 'Payment Succesful',
					text: `Thank you for shopping with PushCart`,
					icon: 'success',
					confirmButtonText: 'OK',
					confirmButtonColor: '#3085d6'
				}).then((result) => {
					if (result.value) {
						window.location.href = "./../../views/transactions.php"
					} 
				})
			} else {
				Swal.fire(
					'Sorry',
					'A Problem Occured',
					'error'
				);
			}
		}
	})
});

//=================================================
// Clear Cart Button Event
//=================================================

$("#clear-cart").click(function(e) {
	e.preventDefault();
	if ($('#cart-count').html() == 0) {
		Swal.fire(
            'Sorry',
            'Your cart is empty',
            'error'
        );
        return;
	}
	$.ajax({
		url: './../../controllers/clear_cart_controller.php',
		type: 'POST',
		success: function(data) {
			emptyCart();
		}
	})
});

//=================================================
// Remove Button Event
//=================================================

$(".remove-item").click(function (){

	const cartItem = $(this).parent().parent().parent();
	const removeid = $(this).data('id');

	$.ajax({
		url: './../../controllers/cart_controller.php',
		type: 'POST',
		data: {
			id: removeid,
			action: "remove_item"
		},
		cache: false,
		success: function(data) {

			const response = $.parseJSON(data);
			const subtotal = response.subtotal;
			const total = response.total;

			$('#td-subtotal').html("&#8369; "+total);
			$('#td-total').html("&#8369; "+total);
			$('#cart-count').html(response.count);


			cartItem.addClass('remove');
			cartItem.on('transitionend', function() {
				$(this).prev().remove()
				$(this).remove()

				// display no items in cart when cart session is unset
				if (response.count === 0) {
					//recreate DIV
					$('#cart-items-list')
					.append(
					'<div class="item-list"><div class="col items-cart"><div class="col"><h2 class="text-center">No items in your cart.</h2></div></div></div>');
				}
			})

		}
	})
});

//=================================================
// Add to Cart Actions
//=================================================

$('.addToCart').click(function(e) {
	e.preventDefault();

	let addData = new FormData();
	itemQty = $(this).parent().prev().children('input').val();
	itemId = $(this).data('id');
	pageUrl = window.location.href


	// create a validation to make sure that quantity is greater than 0

	if (itemQty <= 0) {
		alert('Please Enter Valid Quantity!');
	} else {

		addData.append("productId", itemId);
		addData.append("productQty", itemQty);
		addData.append("pageUrl", pageUrl);
		console.log(addData);

	}

	// check key name and values of form data

	for (var key of addData.entries()) {
        console.log(key[0] + ', ' + key[1]);

    }
	
	//jQuery AJAX (alternative -- working)

    // contentType must be set to false to avoid sending data with contentType header
    // processData must be set to false because jQuery will try to convert your FormData into a string, which will fail.

	$.ajax({
		url: './../../controllers/update_cart.php',
		type: 'POST',
		data: addData,
		contentType: false, 
		cache: false,             
		processData: false,
		success: function(response) {
			console.log(response);
			$('#cart-count').html(response);
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 1200,
				timerProgressBar: true,			
			})

			Toast.fire({
				icon: 'success',
				title: 'Added to Cart'
			});
		},
	});

	//JS fetch api (working)

	// fetch("./../controllers/update_cart.php", {
	// 	method: "POST",
	// 	body: addData
	// })
	// .then( (response) => response.json())
	// .then((data) => {
	// 	console.log(data);
	// 	document.querySelector("#cart-count").innerHTML = data;
	// })
})

$('.shop-minus').click(function(e) {
	
	if ($(this).next().val() == 1) {
		$(this).prop('disabled', true);
		$(this).addClass('disabled');
	} else {
		$(this).prop('disabled', false);
		$(this).removeClass('disabled');
		$(this).next().val(function (i, value) {
			return --value;
		});
		$(this).next().trigger('change');
	}
})

$('.shop-add').click(function(e) {
	
	if ($(this).prev().val() == 100) {
		$(this).prop('disabled', true);
		$(this).addClass('disabled');
		$(this).prev().trigger('change');
	} else {
		$(this).prop('disabled', false);
		$(this).removeClass('disabled');
		$(this).prev().val( function (i, value) {
			return ++value;
		});
		$(this).prev().trigger('change');
	}
})

$('.shop-item-qty').change(function(e) {
	if ($(this).val() <= 1) {
		$(this).prev().prop('disabled', true);
		$(this).prev().addClass('disabled');
	} else {
		$(this).prev().prop('disabled', false);
		$(this).prev().removeClass('disabled');
	};

	if ($(this).val() >= 100) {
		$(this).next().prop('disabled', true);
		$(this).next().addClass('disabled');
	} else {
		$(this).next().prop('disabled', false);
		$(this).next().removeClass('disabled');
	}; 
})

//=================================================
// Functions Declaration
//=================================================

let editQty = () => {

	$.ajax({
		url: './../../controllers/cart_controller.php',
		type: 'POST',
		data: editData,
		cache: false,
		success: function(data) {
			const response = $.parseJSON(data);
			const subtotal = response.subtotal;
			const total = response.total;
			const subprice = response.subprice;

			$('#td-subtotal').html("&#8369; "+total);
			$('#td-total').html("&#8369; "+total);
			$(elSubprice).html("&#8369; "+subprice);
			$('#cart-count').html(response.count);
		}

	})
};

let emptyCart = () => {
  itemContainer = document.querySelectorAll('.item-list');
  horiLine = document.querySelectorAll('.cart');

  itemContainer.forEach((item, index) => {
    setTimeout(()=>{
      item.classList.add("remove")
      item.previousElementSibling.classList.add("remove");
      item.previousElementSibling.remove();
      item.addEventListener('transitionend', function() {
      	item.remove();
      })
    },100*index)
 
  })
  document.getElementById('td-subtotal').innerHTML="&#8369; 0";
  document.getElementById('td-total').innerHTML="&#8369; 0";
  document.getElementById('cart-count').innerHTML='0';
  setTimeout((()=>{
    $('#cart-items-list')
	  	.append(
			'<div class="item-list">'+
				'<div class="col items-cart">'+
					'<div class="col">'+
						'<h2 class="text-center">No items in your cart.</h2>'+
					'</div>'+
				'</div>'+
			'</div>'
		);
    }), 3000)
  
}

