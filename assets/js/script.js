$(document).ready(function() {

//=================================================
// USER NAVIGATION LINKS
//=================================================


    $('#user-navigation').click(function(){
        $(".user-menu").fadeIn(200); 
    });

    $('#user-navigation').mouseenter(function(){
        $(".user-menu").fadeIn(200); 
    });

    $('.user-menu').mouseenter(function(){
        $(".user-menu").fadeIn(50); 
    });

    $('.user-menu, #user-navigation').mouseleave(function(){
        $(".user-menu").fadeOut(100); 
    });

    $('body').click(function (e) {

        if(e.target.id == "#user-navigation")
            return;

        if($(e.target).closest('#user-navigation').length)
            return;  

        $(".user-menu").fadeOut(100);
    })




//=================================================
// TRANSACTION SCRIPTS
//=================================================
//
//=================================================
// Set Price in Top Invoice
//=================================================

totalPriceValue = $('#total-price-footer').html();
$('#total-price-header').html(totalPriceValue);

//=================================================
// INVOKE TREE
//=================================================

$.ajax({
    url: './../../controllers/fetch.php',
    method: 'POST',
    success: function(data) {
        $('#transactionTree').jstree({
            'plugins': ["wholerow", "search"],
            'core': {
                'data': JSON.parse(data),
                'themes': {
                    'name': 'proton',
                    'responsive': true
                }
            }
        });

    }
});

//=================================================
// INVOKE TREE SEARCH AND EVENTS
//=================================================


$(function () {
  var to = false;
  $('#search-list').keyup(function () {
    if(to) { clearTimeout(to); }
    to = setTimeout(function () {
      var v = $('#search-list').val();
      $('#transactionTree').jstree('search', v);
    }, 250);
  });
});

$('#transactionTree').on('search.jstree', function (e, data) { 
    data.instance.select_node(data.res); 
});

$('#transactionTree').on("changed.jstree", function (e, data) {
    Code = $(this).jstree('get_selected', true)
    textCode = $(this).jstree('get_selected', true)[0].text;
    textCodeParent = $(this).jstree('get_selected', true)[0].parent;
    

    if (textCodeParent != "#") {
        $.ajax({
            url: './../../controllers/transaction_actions.php',
            type: 'POST',
            data: {
                code: textCode,
                action: "search"
            },
            cache: false,
            success: function(response) {
                let data = jQuery.parseJSON(response);

                 $('#invoice-container').load('./../../partials/invoice_template.php', data, ()=>{
                        totalPriceValue = $('#total-price-footer').html();
                        $('#total-price-header').html(totalPriceValue);
                    }).hide().fadeIn();
            }
        })
    }
})
$("#transactionTree").on("ready.jstree", function() {
     $(this).jstree('search', $('.transaction-no').html());
    // $(this).jstree('select_node', '5391588425817');
});

//=================================================
// COOKIE INFORMATION MODAL
//=================================================

$(function(){
    if( ! Cookies.get('covid')){
        Swal.fire({
            title: '<strong>PLEASE READ</strong>',
            icon: 'info',
            html:
            'Due to COVID-19, please expect some delays<br><br>' +
            'with the delivery.<br><br>' +
            'Thank You!',
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: true,
            confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> Ok!',
            confirmButtonAriaLabel: 'Thumbs up, OK!',
        });
        Cookies.set('covid', 'yes', { expires: 0.5 });
    }
});

  
//=================================================
// REGISTRATION VALIDATION
//=================================================


    $('#firstname').on('input', function() {
        if ($.trim($(this).val()).length === 0) {
            $(this).siblings("span").html("Required");
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            errorCount++;
            return;
        }
        else {
            $(this).siblings("span").hide();
            errorCount = 0;
        };
    });

    $('#lastname').on('input', function() {
        if ($.trim($(this).val()).length === 0) {
            $(this).siblings("span").html("Required");
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            errorCount++;
            return;
        }
        else {
            $(this).siblings("span").hide();
            errorCount = 0;
        };
        
    });

    $('#email').on('input', function() {
        if ($.trim($(this).val()).length < 5) {
            $(this).siblings("span").html("Incorrect format name@domain.com");
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            errorCount++;
            return;
        }
        
        if ($(this).val().indexOf("@") == -1) {

            $(this).siblings("span").html("must contain \"@\"");
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            errorCount++;
            return;
        };

        let emailVal = $(this).val();
        let emailSplit = emailVal.split("@");
        let emailDot = emailSplit[1].indexOf(".");
        let afterAt = emailSplit[1].length
        let afterDot = emailSplit[1].split(".");
        let afterDotCount = afterDot.length;
        let afterDotCharCount = afterDot[1].length;

        if (emailDot == -1 || emailDot < 2 || afterDotCount > 2 || afterDotCharCount < 2 ) { //|| $(emailDot) < 2 || $(afterDotCount > 2) 

            $(this).siblings("span").html('Incorrect format name@domain.com');
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            isErr = 1;
            return;
        };

        for (var i = 0; i < afterDot.length; i++) {
            if (afterDot[i].length == 0) {
                return ;
            }
        };

        $(this).siblings("span").hide();
        errorCount = 0;
    });

    $('#password').on('input', function() {
        if ($.trim($(this).val()).length < 8) {
            $(this).siblings("span").html("Password must be 8 characters or more");
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            errorCount++;
            return;
        }
        else {
            $(this).siblings("span").hide();
            errorCount = 0;
        };
    });

    $('#confirm_password').on('input', function() {
        if ($(this).val() != $('#password').val()) {
            $(this).siblings("span").html("Password not matched");
            $(this).siblings("span").show();
            $(this).siblings("span").css("background-color", "#FF6961");
            errorCount++;
            return;
        }
        else {
            $(this).siblings("span").hide();
            errorCount = 0;
        };
    });

//=================================================
// REGISTRATION ACTIONS
//=================================================

    var errMsg = 'error';
    let inputForm = $("#registration-form :input.form-control");
    let errorCount = 0;

    $('#registration-form').submit(function(e) {
        e.preventDefault();    
    });

    $("#regBtn").click(function () {
        $(inputForm).each(function() {
            if ($.trim($(this).val()).length === 0) {
                $(this).siblings("span").html("Required");
                $(this).siblings("span").show();
                $(this).siblings("span").css("background-color", "#FF6961");
                errorCount++;
            }
        });

        if (errorCount === 0) {
            $.ajax({
            url: "./../../controllers/process_register.php",
            type: "POST",
            data: {
                firstname: $("#firstname").val(),
                lastname: $("#lastname").val(),
                email: $("#email").val(),
                password: $("#password").val(),
                confirm_password: $("#confirm_password").val()
            },
            cache: false,
            success: function(data){
                let resp = jQuery.parseJSON(data);                    
                if (resp.response == 'success') {
                    regSuccess();
                } else {
                    errMsg = resp.error;
                    regFail();
                }
            },
            error: function(jqXhr, status, exception) {
               console.log('Exception:', exception);
            },
            });
        };
    });

    function regFail() {
        Swal.fire(
            'Registration Failed',
            errMsg,
            'error'
            );
    };
    function regSuccess() {
        Swal.fire({
            title: 'Registration Successful!',
            text: `You may now login.`,
            icon: 'success',
            confirmButtonText: 'OK',
            confirmButtonColor: '#3085d6'
        }).then((result) => {
            if (result.value) {
                window.location.href = "./../../views/login.php"
            }
        })
    };

//=================================================
// PRODUCT ACTIONS
//=================================================

    $("#addProductForm").submit(function (e) {
        e.preventDefault();
        const refresh = window.location.href;
        $.ajax({
            url: "./../../controllers/product_add.php",
            method: "POST",
            data: new FormData(this),
            contentType: false,       
            cache: false,             
            processData:false,
            success: function(response) {
                console.log(response);
                if(response == "success") {
                    Swal.fire({
                        title: "Sucess!",
                        text: "Product has been added in the database.",
                        showCancelButton: false,
                        icon: 'success',
                    })
                    .then((result) => {
                        if(result.value){
                            window.location.href = refresh;
                        } else if(result.dismiss == 'esc') {
                            
                            window.location.href = refresh;

                        } else {
                            
                            window.location.href = refresh;

                        }
                    });
                } else if (response == "multiple") {
                    Swal.fire(
                        'Sorry!',
                        'Product already exists.',
                        'error'
                        )
                } else if (response == "invalid") {
                    Swal.fire(
                        'Sorry!',
                        'All Fields Required / Invalid Input.',
                        'error'
                        )
                } else if (response == "exten_size") {
                    Swal.fire(
                        'Sorry!',
                        'Invalid image type / Imvalid image size.',
                        'error'
                        )
                } else {
                    Swal.fire(
                        'Failed',
                        "Sorry registration failed. Please try again.",
                        'error'
                        )
                };
            }
        })
    });

//=================================================
// LOGIN ACTIONS
//=================================================

    $('#login-form').submit(function(e) {
        e.preventDefault();

        let loginData = new FormData(this);

        loginData.append('jsRequest', true);

        // for (var key of loginData.entries()) {
        //     console.log(key[0] + ', ' + key[1]);

        // }

        $.ajax({
            url: './../../controllers/authenticate.php',
            type: 'POST',
            data: loginData,
            contentType: false,       
            cache: false,             
            processData:false,
            success: function(response) {
                console.log(response);
                if (response == 'success') {
                    logInSuccess();
                    setTimeout(function() {window.location.reload();}, 1100);
                    
                } else if (response == 'user')  {
                    Swal.fire(
                        'Log-in Failed',
                        'User not found. Please check details and try again.',
                        'error'
                        );
                } else if (response == 'password')  {
                    Swal.fire(
                        'Log-in Failed',
                        'Incorrect Password',
                        'error'
                        );
                } else if (response == 'empty')  {
                    Swal.fire(
                        'Log-in Failed',
                        'All fields are required.',
                        'error'
                        );
                } 
            }
        })
    });

    function logInSuccess() {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1100,
            timerProgressBar: true,
    
        })

        Toast.fire({
            icon: 'success',
            title: 'Logged in successfully'
        });
    };

//=================================================
// CATEGORY FUNCTIONS
//=================================================

    var urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has('fail')) {
        if (urlParams.get('fail') == 'delete') {
            Swal.fire({
                title: "Sorry!",
                text: urlParams.get('categ') + ' is currently in use and can not be deleted.',
                showCancelButton: false,
                icon: 'error',
            })
            .then((result) => {
                if(result.value){
                    window.history.pushState('', 'Login Success', '/views/add_category.php');
                } else if(result.dismiss == 'esc') {
                    
                    window.history.pushState('', 'Login Success', '/views/add_category.php');

                } else {
                    
                    window.history.pushState('', 'Login Success', '/views/add_category.php');

                }
            });
        }
    }

    $("#addCategBtn").click(function (e) {
        e.preventDefault();
        const refresh = window.location.href;
        $.ajax({
            url: "./../../controllers/process_add_category.php",
            method: "POST",
            data: {
                name: $("#add-categ-name").val()
            },
            success: function(response) {
                console.log(response);
                if(response == "success") {
                    Swal.fire({
                        title: "Sucess!",
                        text: $("#add-categ-name").val() + " category has been added in the database.",
                        showCancelButton: false,
                        icon: 'success',
                    })
                    .then((result) => {
                        if(result.value){
                            window.location.href = refresh;
                        } else if(result.dismiss == 'esc') {
                            
                            window.location.href = refresh;

                        } else {
                            
                            window.location.href = refresh;

                        }
                    });
                } else if (response == "multiple") {
                    Swal.fire(
                        'Sorry!',
                        $("#add-categ-name").val() + ' category already exists.',
                        'error'
                        )
                } else {
                    Swal.fire(
                        'error',
                        "Please check input and try again",
                        'error'
                        )
                };
            }
        })
    });

    var editCategId;

    $(".editCategBtn").click( function () {
        editCategId = $(this).data('href');
        $(".modal-body #categId").val( editCategId );
    });


    $("#goEditCateg").click(function (e) {
        e.preventDefault();
        const refresh = window.location.href;
        $.ajax({
            url: "./../../controllers/edit_categ.php",
            method: "POST",
            data: {
                name: $("#new-name").val(),
                id: editCategId
            },
            success: function(response) {
                console.log(response);
                if(response == "success") {
                    Swal.fire({
                        title: "Sucess!",
                        text: "Category updated to " + $("#new-name").val(),
                        showCancelButton: false,
                        icon: 'success',
                    })
                    .then((result) => {
                        if(result.value){
                            window.location.href = refresh;
                        } else if(result.dismiss == 'esc') {
                            
                            window.location.href = refresh;

                        } else {
                            
                            window.location.href = refresh;

                        }
                    });
                    
                } else if (response == "multiple") {
                    Swal.fire(
                        'Sorry!',
                        $("#new-name").val() + ' category already exists.',
                        'error'
                        )
                } else {
                    Swal.fire(
                        'error',
                        "Please check input and try again",
                        'error'
                        )
                };
            }
        })
    });
    var deleteId;
    var deleteName;

    $('.delCategBtn').click(function(e) {
        e.preventDefault();
        deleteId = $(this).data('href');
        deleteName = $(this).data('categ');
        const refresh = window.location.href;

        $.ajax({
            url: './../../controllers/delete_categ.php',
            type: 'POST',
            data: {
                id: deleteId,
                name: deleteName
            },
            success: function(data) {
                let categ = $.parseJSON(data);
                if (categ.response == 'fail') {
                    Swal.fire({
                        title: "Sorry!",
                        text: categ.name + ' is currently in use and can not be deleted.',
                        showCancelButton: false,
                        icon: 'error',
                    })
                    .then((result) => {
                        if(result.value){
                            window.location.href = refresh;
                        } else if(result.dismiss == 'esc') {
                            
                            window.location.href = refresh;
        
                        } else {
                            
                            window.location.href = refresh;
        
                        }
                    });
                } else if (categ.response == 'success') {
                    Swal.fire({
                        title: "Success!",
                        text: categ.name + ' has been deleted.',
                        showCancelButton: false,
                        icon: 'success',
                    })
                    .then((result) => {
                        if(result.value){
                            window.location.href = refresh;
                        } else if(result.dismiss == 'esc') {
                            
                            window.location.href = refresh;
        
                        } else {
                            
                            window.location.href = refresh;
        
                        }
                    });
                }
            }
        })
    })
});