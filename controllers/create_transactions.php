<?php 
	// START PHP MAILER
	//======================================================
	

	
	//======================================================
	// END PHP MAILER

	session_start();


	//database connection
	
	require_once "./connection.php";

	// control structure to check user session

	if (!isset($_SESSION['user'])) {
		header("location: ./../views/login.php");
	}

	// Check if Cart Session exists else return to shop

	if ( !isset($_SESSION['cart']) || count($_SESSION['cart']) == 0) {
		header("location: ./../views/catalogue.php");
	}

	// die(var_dump($check));
	$total = 0;
	$product_id = join(",", array_keys($_SESSION['cart']));	
	

	//query all products
	$sql_get_cart = "SELECT * FROM products WHERE id IN ($product_id)";

	$result = mysqli_query($conn, $sql_get_cart);

	if (!$result) {
		echo "fail";
		return;
	}

	while ($product = mysqli_fetch_assoc($result)) {
		$subtotal = $product['price'] * $_SESSION['cart'][$product['id']];
		$total += $subtotal;
	}

	//customer and transaction details
	$transaction_code = createTransactionCode();
	// $transaction_codes = strtoupper(uniqid());
	$user_id = $_SESSION['user_id'];

	$payment_mode_id = $_POST['payment-mode'];
	$status_id = 1;

	
	function createTransactionCode () {
		$transaction_code = "";
		$chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D'];

		for ($i=0; $i < 5; $i++){
			$index = rand(0,15);

			$transaction_code .= $chars[$index];

		};

		$transaction_code = $transaction_code.getdate()[0];
		return $transaction_code;

	};

	//query to add transaction

	$sql_add_transaction = 
	"INSERT INTO transactions (transaction_code, total, user_id, status_id, payment_mode_id) VALUES ('$transaction_code', $total, $user_id, $status_id, $payment_mode_id)";

	$result = mysqli_query($conn, $sql_add_transaction);

	$transaction_id = mysqli_insert_id($conn);

	$array_entries = [];


	foreach ($_SESSION['cart'] as $id => $quantity) {
		$array_entries[] = "($transaction_id, $id, $quantity)";
	};

	$values = join(",",$array_entries);
	$query = "INSERT INTO product_transactions (transaction_id, product_id, quantity) VALUES $values";

	$result = mysqli_query($conn, $query);

	//==============================================
	// Start of send email to customer
	//==============================================

	// phpmailert3st3r@gmail.com
	// walongaskterisk
	//==============================================
	// End of send email to customer
	//==============================================

	
	if ($result) {
		echo "success";
		unset($_SESSION['cart']);
	} else {
		echo "fail";
	}

	// header("location: ./../views/transactions.php");

?>