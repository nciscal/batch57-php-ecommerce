<?php

	require 'connection.php';

	$sql_query = "DELETE FROM categories WHERE id = {$_POST['id']}";

	$result = mysqli_query($conn, $sql_query);


	if ($result) {
		$data["response"] = "success";
		$data["name"] = $_POST['name'];
		echo json_encode($data);
		// header("location: ./../views/add_category.php");
	} else {
		$query_error = mysqli_error($conn);
		$data["response"] = "fail";
		$data["name"] = $_POST['name'];
		echo json_encode($data);
		// header("location: ./../views/add_category.php?fail=delete&categ={$_GET['categ']}");
	}

	