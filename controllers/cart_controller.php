<?php
	// Start session
	session_start();
	require 'connection.php';


	// Catch variables from POST
	$id = $_POST['id'];

	
	//========================================================================//
	// DO THIS IF ACTION IS SET, ELSE JS IS DISABLED
	// JUST TESTING SOME WORKAROUNDS
	//========================================================================//

	if (isset($_POST['action'])) {

		if ($_POST['action'] == "edit_quantity") {

			$qty = $_POST['quantity'];

			//=============================
			// Edit item quantity in cart
			//=============================

			$_SESSION['cart'][$id] = $qty;

			//update subtotal price per product
			$individual_subtotal_query = "SELECT * FROM products WHERE id = $id";
			$individual_subtotal_result = mysqli_query($conn, $individual_subtotal_query);
			$individual_subtotal = mysqli_fetch_assoc($individual_subtotal_result);
			extract($individual_subtotal);


			// Generate Data for updating quantity
			$data = generateData();

			$data['subprice'] = number_format(($qty * $price) , 2);
			echo json_encode($data);


		} else {

			//=============================
			// Remove item from cart
			//=============================

			// Destroy product @session cart

			unset($_SESSION['cart'][$id]);

			// Control structure that will check if 0 items will be in CART SESSION.

			if (count($_SESSION['cart']) === 0) {
				unset($_SESSION['cart']);
				$data["subtotal"] = 0;
				$data["total"] = 0;
				$data["count"] = 0;
				echo json_encode($data);
			} else {

				//recalculate subtotal and total
				$data = generateData();
				echo json_encode($data);

			}
		}

	} else {

		echo "Sorry this site utilizes Javascript. Please enable Javascript in your browser.";
	}

function generateData()  {

	require 'connection.php';
	$total = 0;

	foreach($_SESSION['cart'] as $id => $qty) {

		// create a query that will insert from products table the product that matches the product id.

		$sql_query = "SELECT * FROM products WHERE id = $id";

		// var_dump($sql_query);

		$result = mysqli_query($conn, $sql_query);

		$item = mysqli_fetch_assoc($result);

		// convert the assoc array into a number of variable with names as the keys;
		extract($item);

		$subtotal = $price * $qty;
		$total += $subtotal;
	};

		$data["subtotal"] = number_format($subtotal,2);
		$data["total"] = number_format($total, 2);
		$data["count"] = array_sum($_SESSION['cart']);

		return $data;
}