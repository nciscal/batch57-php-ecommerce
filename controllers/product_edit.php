<?php 

	
// catch product id
$product_id = $_GET['id'];

//store POST data
//htmlspecialchars() converts special char to HTML entities
//trim() remove white spaces

$product_name = htmlspecialchars(trim($_POST['product-name']));

$product_price = htmlspecialchars(trim($_POST['product-price']));

$product_category = htmlspecialchars(trim($_POST['product-category']));

$product_description = htmlspecialchars(trim($_POST['product-description']));


//validate the stored data

function isFileSelected($file) {
	if(empty($_FILES['product-image']['name'])) {
		return false;
	} else {
		return true;
	}
};


// function to check file type and size of image

function saveFile ($file) {
	$file_name = $file['name'];
	$file_size = $file['size'];
	$file_type = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
	$file_tmp_name = $file['tmp_name'];
	$extensions = array("jpg", "jpeg", "png", "gif");



	//validate file type
	$is_not_image = true;

	$is_not_image = (in_array($file_type, $extensions)) ? false : true;

	// validate file size

	$is_file_empty = true;

	$is_file_empty = ($file_size>0 && $file_size <800000) ? false : true;


	if (!$is_file_empty && !$is_not_image) {

		//delete old image

		$img_dir = "./../assets/images/";
		$img_to_delete = $img_dir.$_GET['img'];
		
		unlink($img_to_delete);

		//set DIR for image to be uploaded
		$destination = "./../assets/images/$file_name";
		move_uploaded_file($file_tmp_name, $destination);

		return $file_name;

	} else {
		echo "<script>alert('Error Image Upload')</script>";
	}
};

function isInputComplete($name,$price,$category,$description) {
	$complete = (empty($name)||empty($price)||empty($category)||empty($description)) ? false : true;
	return $complete;
}

if(isInputComplete($product_name,$product_price,$product_category,$product_description)) {
	require 'connection.php';

	if (isFileSelected($_FILES['product-image'])) {
		$image = saveFile($_FILES['product-image']);

	// define query to update product
		$sql_update_product = "UPDATE products SET
		name = '{$product_name}',
		price = '{$product_price}',
		category_id = {$product_category},
		image = '$image'
		WHERE
		id = {$product_id}";


	//connect to database
		mysqli_query($conn, $sql_update_product);

	//redirect if succesful
		header("location: ./../views/catalogue.php?page=1&category_id=$product_category");

	} else {
	
	// define query to update product without image
		$sql_update_product = "UPDATE products SET
		name = '{$product_name}',
		price = '{$product_price}',
		category_id = {$product_category}
		WHERE
		id = {$product_id}";

		// echo $sql_update_product;

	//connect to database
		mysqli_query($conn, $sql_update_product);

	//redirect if succesful
		header("location: ./../views/catalogue.php?page=1&category_id=$product_category");
	}
} else {
	// echo "HELLO";
	// die();
	header("Location: {$_SERVER['HTTP_REFERER']}");
}

?>