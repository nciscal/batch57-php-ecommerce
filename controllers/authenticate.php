<?php 
	
	require_once 'connection.php';

	
	session_start();
	if(isset($_SESSION["user"]))
	{
		header("Location: ./../views/home.php");
	}

	$email = $_POST['email'];
	$password = $_POST["password"];
	$is_js = isset($_POST["jsRequest"]) ? true : false;


	if (empty($email) || empty($password)) 
	{
		if (!$is_js) 
		{
			header("Location: ./../views/login.php?logsucc=false");
		}
		else
		{
			echo "empty";	
		}
	}	
	else
	{
		$sql_query = "SELECT * FROM users WHERE `email` = '$email'";
		$result = mysqli_query($conn,$sql_query);
		if (mysqli_num_rows($result) > 0)
		{
			while($row = mysqli_fetch_array($result))
			{

				if(password_verify($password, $row["password"]))
				{
					if ($row["role_id"] == 1)
					{
							// return true admin;
						$_SESSION["user"] = $row["firstname"];
						$_SESSION["role"] = $row["role_id"];
						$_SESSION["user_id"] = $row["id"];

						if (!$is_js) 
						{
							header("Location: ./../views/login.php?logsucc=true");
						}
						else
						{
							echo "success";	
						}
					} 
					else
					{
						$_SESSION["user"] = $row["firstname"];
						$_SESSION["role"] = $row["role_id"];
						$_SESSION["user_id"] = $row["id"];
						if (!$is_js) 
						{
							header("Location: ./../views/login.php?logsucc=true");
						}
						else
						{
							echo "success";	
						}
					}

				}
				else
				{
					// if false;
					if (!$is_js) 
					{
						header("Location: ./../views/login.php?logsucc=false");
					}
					else
					{
						echo "password";	
					}

				}
			}
		}
		else
		{
			
			if (!$is_js) 
			{
				header("Location: ./../views/login.php?logsucc=false");
			}
			else
			{
				echo "user";	
			}
		}

	}
?>