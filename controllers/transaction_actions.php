<?php

require './connection.php';

//==============================
// STORE ALL POST DATA
//==============================

$action = $_POST['action'];
$transaction_code = $_POST['code'];



//==============================
// SET QUERIES HERE
//==============================


// $get_transaction_query = "SELECT * FROM transactions WHERE transaction_code = '{$transaction_code}'";

$get_transaction_query = "SELECT 
	transactions.transaction_code, 
	transactions.total,
	transactions.date, 
	users.firstname, 
	users.lastname,
	payment_modes.name as payment_name,
	statuses.name as status_name,
	transactions.id
	FROM transactions
	JOIN users ON (transactions.user_id = users.id)
	JOIN payment_modes ON (transactions.payment_mode_id = payment_modes.id)
	JOIN statuses ON (transactions.status_id = statuses.id)
	WHERE transactions.transaction_code = '{$transaction_code}'";



//==============================
// DETERMINE ACTION
// EXECUTE FUNCTION
//==============================


switch ($action) {
	case "search":

		get_transaction($transaction_code, $get_transaction_query, $conn);
		break;
}


//==============================
// SET FUNCTIONS HERE
//==============================

function get_transaction($transaction_code, $get_transaction_query, $conn) {

	$get_transaction = mysqli_query($conn, $get_transaction_query);

	$transaction = mysqli_fetch_assoc($get_transaction);

	echo json_encode($transaction);

}