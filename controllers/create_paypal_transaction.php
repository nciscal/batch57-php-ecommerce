<?php

	require_once 'connection.php';

	session_start();

	$product_ids = join(",",array_keys($_SESSION['cart']));
	$sql_query_items = "SELECT * FROM products WHERE id IN ($product_ids)";
	$success = "success";
	$fail = "fail";

	$result = mysqli_query($conn, $sql_query_items);

	$total = 0;
	while ($item = mysqli_fetch_assoc($result)) {
		$subtotal =  $item['price'] * $_SESSION['cart'][$item['id']];
		$total += $subtotal;
	}


// transactions table
	
	// transaction code <= orderID paypal
	$transaction_code = $_POST['orderID'];

	// user_id <= SESSION
	$user_id = $_SESSION['user_id'];
	
	// payment mode
	$payment_mode_id = 3;

	// status_id <= by default 1
	$status_id = 1;

	$sql_insert_transaction_query = "INSERT INTO transactions (transaction_code, total, user_id, payment_mode_id, status_id) VALUES ('$transaction_code', '$total', '$user_id', '$payment_mode_id', '$status_id')";

	
 	$result_insert_transaction_query = mysqli_query($conn, $sql_insert_transaction_query);

 	$transaction_id = mysqli_insert_id($conn);

 	// For Product Transactions
	
	$array_entries = [];

	foreach ($_SESSION['cart'] as $id => $quantity) {
		$array_entries[] = "($transaction_id, $id, $quantity)";
	};


	// multiple insert

	$values = join(",",$array_entries);

	$query = "INSERT INTO product_transactions (transaction_id, product_id, quantity) VALUES $values";

	$result = mysqli_query($conn, $query);


	if ($result) {
		unset($_SESSION['cart']);
		echo $success;
	} else {
		echo $fail;
	}

	// header("location: ./../views/transactions.php");

?>


