<?php 

	require_once 'connection.php';
	session_start();

	function getCartCount() {
		return array_sum($_SESSION['cart']);
	}

	$product_id = $_POST['productId'];
	$product_qty = $_POST['productQty'];
	$_SESSION['shop-url'] = $_POST['pageUrl'];

	if (isset($_SESSION['cart'][$product_id])) {

		$_SESSION['cart'][$product_id] += $product_qty;

		echo array_sum($_SESSION['cart']);

	} else {
		// declare a session var named cart with key = received product id
		//catalog_fetch req

		$_SESSION['cart'][$product_id] = $product_qty;
		echo array_sum($_SESSION['cart']);
	}
?>