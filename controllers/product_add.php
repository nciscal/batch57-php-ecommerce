<?php
require 'connection.php';

$name = $_POST["product-name"];
$price = $_POST["product-price"];
$description = $_POST["product-description"];
$category_id = $_POST["product-category"];

$tmp_name = $_FILES["product-image"]["tmp_name"];
$image = $_FILES["product-image"]["name"];
$image_size = $_FILES["product-image"]["size"];
$ext = strtolower(pathinfo($image, PATHINFO_EXTENSION));

$extensions = array("jpg", "jpeg", "png", "gif");

$valid_img_size = false;

// $img_dir = "./../assets/images/product_images/";
$img_dir = "./../assets/images/";

if ($image_size > 0 && $image_size < 800000) {
    $valid_img_size = true;
}

if (
    empty($name) ||
    empty($price) ||
    empty($description) ||
    empty($image) ||
    empty($category_id) ||
    !is_numeric($price)
) {
    echo 'invalid';
} else {
    $find_query = "SELECT * FROM products WHERE name = '$name'";
    $find_categ = mysqli_query($conn, $find_query);

    if (mysqli_num_rows($find_categ) > 0) {
        echo 'multiple';
    } else {
        if (!in_array($ext, $extensions) || !$valid_img_size) {
            echo "exten_size";
        } else {
            // Move image
            //check directory if exist
            if (!is_dir($img_dir)) {
                //Create directory if it does not exist
                mkdir($img_dir);
            }
            $setdate = new DateTime('now', new DateTimezone('Asia/Singapore'));
            $dateUpload = date_format($setdate, 'Y-m-d-h-i-s');

            $new_image = $dateUpload . "_" . $image;

            move_uploaded_file($tmp_name, $img_dir . $new_image);

            // Store to database
            $query = "INSERT INTO products (name, price, description, image, category_id) VALUES ('$name','$price', '$description', '$new_image', '$category_id')";

            $result = mysqli_query($conn, $query);

            if ($result) {
                echo "success";
            } else {
                echo "fail";
            }
        }
    }
}
