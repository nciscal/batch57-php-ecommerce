<?php 

session_start();
//connect to database
require_once './../controllers/connection.php';


$get_last_transaction = "SELECT MAX(id) as last_transac FROM transactions";

$last_transaction = mysqli_fetch_assoc(mysqli_query($conn, $get_last_transaction));

$get_transactions = "SELECT * FROM transactions WHERE user_id = {$_SESSION['user_id']} ORDER BY date DESC";

$transactions = mysqli_query($conn, $get_transactions);

$transactions_count = mysqli_num_rows($transactions);

$transac_date = "";
$i = -1;


//===========================================================================

while ($transaction = mysqli_fetch_assoc($transactions)) 
{
	$new_transac_date=substr($transaction['date'], 0, strpos($transaction['date'], ' '));
	$is_same_date = ($transac_date === $new_transac_date) ? true : false;
	$invoice_icon = ($transaction['payment_mode_id'] == 3) ? "./../assets/images/icons/paypal-invoice.png" : "./../assets/images/icons/normal-invoice.png";
	

		if (!$is_same_date) {
		
			unset($transaction_date);
			unset($transaction_date_data);
			$transaction_date['text'] = $new_transac_date;
			$transaction_date_data['text'] = $transaction['transaction_code'];
			$transaction_date_data['icon'] = $invoice_icon;
			$transaction_date['children'][] = $transaction_date_data;

			// append to data array
			$data[] = $transaction_date;
			$i++;

		} else {
			
			$transaction_date_data['text'] = $transaction['transaction_code'];
			$transaction_date_data['icon'] = $invoice_icon;

			$data[$i]['children'][] = $transaction_date_data;
			
		}

		$transac_date = $new_transac_date;	
};

// echo "<pre>" , print_r($data) , "</pre>";

echo json_encode($data);


?>