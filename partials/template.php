<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="./../assets/themes/proton/style.css" />
    <link rel="shortcut icon" type="image/x-icon" href="./../assets/images/favicon.png">
    
   
    <link rel="stylesheet" href="./../assets/css/style.css">

    <title>PushCart</title>

    
</head>
<body>

    
    
    <div class="d-flex flex-column cont-bg min-vh-100 p-0">
        <div class="row header m-0">
            <?php require_once 'header.php'; ?>
        </div>
        <div class="row flex-fill align-items-center m-0 p-0 ">
            <?php get_content(); ?>
        </div>
        <div class="row footer m-0">
            <?php require_once 'footer.php';?>
        </div>
    </div>




  
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/80fc3687d5.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script src="./../assets/js/jstree.js"></script>
    <script src="./../assets/js/script.js"></script>
    <script src="./../assets/js/delete-product.js"></script>
    <script src="./../assets/js/cart_actions.js"></script>

</body>
</html>