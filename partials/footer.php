<div class="col-lg-12 footer p-0">
	<footer id="main-footer" class="bg-dark">
	    <p class="text-center py-4 text-white my-0">
	        Copyright &copy; <i>PushCart.</i> All Rights Reserved
	    </p>
	</footer>
</div>