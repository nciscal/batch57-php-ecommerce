<div class="col-lg-12 p-0">
    <div class="nav-header text-center">
        <a class="header-logo" href="./../views/home.php">Push<span>Cart</span></a>

            
    </div>
    <div class="nav-right">
        <div class="user-menu">
            <div class="arrow"></div>
            <div class="user-control col-auto">

                <?php if (!isset($_SESSION["user"])) { ?>

                <div class="user-links">
                    <a href="./../views/login.php">
                        Login
                    </a>
                </div>

                <?php 
                } else { 
                ?>

                <div class="user-links">
                    <a href="./../views/transactions.php">Transactions</a>
                    <a href="./../views/logout.php">Logout</a>
                </div>

            <?php } ?>

            </div>
        </div>
        <div class="right-cont">
            <?php if (!isset($_SESSION["user"])) { ?>

                <a href="./../views/login.php">
                    <img src="./../assets/images/user.svg" class="user-logo">
                    Login
                </a>
                <a href="./../views/cart.php">
                    <img src="./../assets/images/market.png" class="cart-logo">
                    Cart [
                    <span id="cart-count">

                    <?php 
                        if (isset($_SESSION['cart'])) {
                            echo array_sum($_SESSION['cart']);
                        } else {
                            echo "0";
                        }
                    ?> 

                    </span> ]
                </a>
        </div>

            <?php } else { ?>
                <div id="user-navigation" >
                    <p id="welcome-user" class="my-auto p-0">
                        <img src="./../assets/images/user.svg" class="user-logo">
                        <span class="mr-2"><strong><?php print $_SESSION["user"]; ?></strong></span>
                    </p>
                </div>
                <img src="./../assets/images/market.png" class="cart-logo">
                <a href="./../views/cart.php">
                    Cart[
                    <span id="cart-count">
                    <?php 
                    if (isset($_SESSION['cart'])) {
                        echo array_sum($_SESSION['cart']);
                    } else {
                        echo "0";
                    }
                    ?> 
                    </span> ]
                </a>
        </div>
            <?php }; ?>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav col-auto mx-auto mt-4">
                <a class="nav-item nav-link active" href="./../views/home.php">Home <span class="sr-only">(current)</span></a>

                <?php  
                if (isset($_SESSION["role"])) {
                    if ($_SESSION["role"] == 1) {?>
                        <a class="nav-item nav-link" href="./../views/add_category.php">Add Category</a>
                        <a class="nav-item nav-link" href="./../views/add_product.php">Add Product</a>
                        <?php    
                    };
                };
                ?>
                <a class="nav-item nav-link" href="./../views/catalogue.php">Shop</a>
                <a class="nav-item nav-link" href="./../views/register.php">Register</a>
            </div>
        </div>
    </nav>
</div>