<?php 
	require_once './../partials/template.php';

	function get_content() {

		//connect to database
		require './../controllers/connection.php';

		$get_transaction_query = "SELECT 
		transactions.transaction_code, 
		transactions.total,
		transactions.date, 
		users.firstname, 
		users.lastname,
		payment_modes.name as payment_name,
		statuses.name as status_name,
		transactions.id
		FROM transactions
		JOIN users ON (transactions.user_id = users.id)
		JOIN payment_modes ON (transactions.payment_mode_id = payment_modes.id)
		JOIN statuses ON (transactions.status_id = statuses.id)
		WHERE users.id = {$_SESSION['user_id']} ORDER BY transactions.id DESC LIMIT 1
		";

		$get_transaction = mysqli_query($conn, $get_transaction_query);

		$transaction = mysqli_fetch_assoc($get_transaction);

		$invoice_no;
		$total = 0;

		$invoice_no = $transaction['id'];
		$transaction_code = $transaction['transaction_code'];
		$transaction_date = substr($transaction['date'], 0, strpos($transaction['date'], ' '));
		$transaction_customer_firstname = $transaction['firstname'];
		$transaction_customer_lastname = $transaction['lastname'];
		$transaction_payment_mode = $transaction['payment_name'];
		$transaction_status = $transaction['status_name'];


		function get_product_transaction($invoice_no, $total) {
			require './../controllers/connection.php';

			$product_transaction_query = "SELECT
			product_transactions.quantity,
			products.name,
			products.price
			FROM product_transactions
			JOIN products ON (products.id = product_transactions.product_id)
			WHERE product_transactions.transaction_id = $invoice_no";


			$product_transaction = mysqli_query($conn, $product_transaction_query);


			$product_array = [];
			
			while ($product_list = mysqli_fetch_assoc($product_transaction)) {
				$price = number_format($product_list['price'],2);
				$amount = $product_list['price'] * $product_list['quantity'];
				$subtotal = number_format($amount, 2);
				$total += $amount;

				echo 
				("<tr>
					<td>{$product_list['quantity']}</td>
					<td>{$product_list['name']}</td>
					<td>&#8369; {$price}</td>
					<td>&#8369; {$subtotal}</td>
				</tr>");
			
			}
			return $total;
		}	
		
		?>

		<div class="container-fluid m-3">
			<div class="row w-100 m-0 mb-5">
				<div class="col-12 text-center">
					<h2>Transactions</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mr-5">
					<h5>Invoice List</h5>
					<input type="text" id="search-list" placeholder="Search Transaction Code" class="form-control my-3">
					<div id="transactionTree">
					</div>
				</div>
				<div id="invoice-container" class="col-lg-8 p-4 border">
					<div class="row">
						<div class="col">
							<img class="invoice-logo" src="./../assets/images/favicon.png">
							<h2 class="invoice-brand d-inline-block">PushCart</h2>
						</div>
						<div class="col invoice-details">
							<h4>INVOICE #<span class="invoice-no"><?=$invoice_no?></span></h4>
							<h5>Transaction Code:<span class="transaction-no"><?= $transaction_code ?></span></h5>
							<h5>Date:<span class="invoice-date"><?= $transaction_date ?></span></h5>
							<h3>Amount Payable<span id="total-price-header" class="invoice-total">&#8369;</span></h3>
						</div>
					</div>
					<hr class="cart">
					<div class="row flex-column">
						<div class="container-fluid mb-3">
							<h4>BILL TO:</h4>
						</div>
						<h5 class="customer-name ml-5"><?= $transaction_customer_firstname . " " . $transaction_customer_lastname ?></h5>
						<h5 class="payment-mode ml-5"><?= $transaction_payment_mode ?></h5>
						<h5 class="transaction-status ml-5"><?= $transaction_status ?></h5>
					</div>
					<hr class="cart">
					<div class="row mt-2">
						<div class="col">
							<div class="table-responsive">
								<table id="invoice-table" class="table table-hover text-center">
									<thead class="bg-dark text-white">
										<tr>
											<th>Quantity</th>
											<th>Product</th>
											<th>Price per unit</th>
											<th>Subtotal</th>
										</tr>
									</thead>
									<tbody>
										<?php $total = get_product_transaction($invoice_no, $total); ?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan="2"></th>
											<th class="text-center bg-dark text-white">Total:</th>
											<th id="total-price-footer" class="bg-dark text-white">&#8369; <?= number_format($total, 2) ?></th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php }; ?>