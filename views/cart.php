<?php
	require_once './../partials/template.php';


	function get_content() {

		//Check if User Login

		if (!isset($_SESSION["user"]) || !isset($_SESSION["role"])) {
			header("location: login.php");
		}

		// catch last shop url to assign to back to shopping href
		//default href if SESSION for href does not exist
		$back_href = "./catalogue.php";

		if (isset($_SESSION['shop-url'])) {
			$back_href = $_SESSION['shop-url'];
		}

		?>
		

		<div class="container my-4">
			<div class="row">
				<div class="col-lg-12 text-center my-4">
					<h2>Your Cart</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="cart-items-container">
						<div class="row">
							<div id="cart-items-list" class="col-lg-8">
								<a href="<?=$back_href?>" class="back-shop">
									<img class="back-shop-img" src="./../assets/images/back.svg" alt="">
									Back to Shopping
								</a>
								<?php
								require_once './../controllers/connection.php';?>
								<?php if(!isset($_SESSION['cart'])) {?>
								<div class="item-list">
									<div class="col items-cart">
											<div class="col">
												<h2 class="text-center">
													No items in your cart.
												</h2>
											</div>
									</div>
								</div>
							<?php }; ?>
								<?php

								if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) {
									$total = 0;
									foreach($_SESSION['cart'] as $product_id => $product_qty) {

										// create a query that will insert from products table the product that matches the product id.

										$sql_query = "SELECT * FROM products WHERE id = $product_id";

										// var_dump($sql_query);

										$result = mysqli_query($conn, $sql_query);

										$item = mysqli_fetch_assoc($result);

										// convert the assoc array into a number of variable with names as the keys;
										extract($item);

										$subtotal = $price * $product_qty;
										$total += $subtotal;

										//variables available will now be
											//id
											//name
											//description
											//image
											//price
											//category_id	
										?>
								<hr class="cart">
								<div class="item-list">
									<img src="./../assets/images/<?= $image?>" class="product-thumb">
									<div class="col items-cart">
										<div class="product-remove">
											<button data-id="<?=$id?>" class="text-center remove-item">X</button>
										</div>
										<div class="product-name">
											<p><?=$name?></p>
										</div>
										<div class="product-description">
											<p><?=$description?></p>
										</div>
										<!-- start edit quantity -->
										<div class="product-qty my-3">
											<form action="./../controllers/remove_from_cart.php" method="POST" class="edit-cart-qty-form">
											<input type="hidden" name="id" class="product-id" value="<?=$id?>">
											<!-- minus button -->
											<div class="cart-edit-qty mx-2">
												<button type="button" class="cart-minus disabled" disabled>-</button>
											<!-- input -->
											</div>
											<input type="number" name="quantity" min="1" max="100" value='<?=$product_qty?>' class="qtyinput">
											<!-- add button -->
											<div class="cart-edit-qty mx-2">
												<button type="button" class="cart-add">+</button>
											</div>
											</form>
										</div>
										<!-- end edit quantity -->
										<div class="product-price">
											<p id="subprice<?=$id?>" class="text-right">₱ <?=number_format($subtotal,2)?></p>
										</div>
									</div>
								</div>
							<?php 
							}; 
							};
							?>
							</div>
							<div class="col-lg-4">
								<table id="cart-items" class="table table-striped table-border table-info">
									<thead>
										<tr class="text-center">
											<th class="bg-dark text-white" colspan="2">SUMMARY</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="cart-summ-left">Subtotal</td>
											<td id="td-subtotal" class="cart-summ-right">₱ <?php if (isset($total)) { echo number_format($total,2);}
												else {
													echo "0";
												}
											?></td>
										</tr>
										<tr>
											<td class="cart-summ-left">Shipping</td>
											<td class="cart-summ-right">FREE</td>
										</tr>
										<tr>
											<td class="cart-summ-left">Est Total</td>
											<td id="td-total" class="cart-summ-right">₱<span id="total-amount"><?php if (isset($total)) { echo number_format($total,2);}
												else {
													echo "0";
												}?></span>
										</td>
										</tr>
										<tr>
											<td colspan="2">
												<form id="payment-mode-form" action="./../controllers/create_transactions.php" method="POST">
													<div class="form-group">
														<label for="payment-mode">Choose Payment Mode:</label>
														<select name="payment-mode" id="payment-mode" class="form-control">
															<?php  
																$sql = "SELECT * FROM payment_modes WHERE id != 3";
																$result = mysqli_query($conn, $sql);
																while($mode = mysqli_fetch_assoc($result)) {?>
																	<option value="<?=$mode['id']?>"><?=$mode['name']?></option>									
																<?php };
															?>
														</select>
													</div>
												</form>
											</td>
										</tr>
									</tbody>
								</table>
								
								<div class="col-100">
									<button id="checkOutBtn" class="btn btn-success text-center w-100">Check Out</button>
								</div>
								<div class="col-100 mt-3">
									<button id="clear-cart" href="./../controllers/clear_cart_controller.php" class="btn btn-danger text-center text-white w-100">Clear Cart</button>
								</div>
								
								<div class="col-100 my-2 text-center">
									<h3>OR</h3>
								</div>

								<div id="paypal-button-container" class="col-100">
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<script
    src="https://www.paypal.com/sdk/js?client-id=AeLTV7ZQdTWjrbZq-nZaK_6aU9rkfu1yw9075Cl3nyj5bGpPWGLu91AQZ90vYzbrZA0kCpAo6_-J8HrH&currency=PHP"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
</script>
	<?php
	};
	?>