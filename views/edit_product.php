<?php

	require_once './../partials/template.php';

	function get_content() {
		require './../controllers/connection.php';

		$id = $_GET['id'];
		$sql_select_single_query = "SELECT * FROM products WHERE id = {$id}";

		$result = mysqli_query($conn, $sql_select_single_query);

		$product = mysqli_fetch_assoc($result);

		?>
		<!-- EDIT FORM HERE -->
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center my-5">
					<h1>Edit Product</h1>	
				</div>
				<div class="col-lg-12">
					<div class="row justify-content-center mb-5">
						<div class="col-auto">
							<div class="d-flex flex-column">
								<img id="product-image-preview" src="./../assets/images/<?= $product['image'] ?>" alt="product image">
							</div>
						</div>
						<div class="col-auto">
								<form action="./../controllers/product_edit.php?id=<?= $product['id'].'&img='.$product['image'] ; ?>" method="post" enctype="multipart/form-data" id="edit-Form">
				
								<!-- name -->
								<div class="d-flex">
									<input type="text" name="product-name" id="edit-product-name" class="mb-2 edit-product-name w-100" value="<?= $product['name']; ?>">
									<i class="far fa-edit"></i>
								</div>

								<!-- price -->
								<div class="d-flex">
								<input type="text" name="product-price" id="edit-product-price" class="mb-2 edit-product-price w-100" data-type="currency" value="<?= $product['price']; ?>" placeholder="₱0"><i class="far fa-edit"></i>
								</div>
								

								<!-- description -->
								<div class="d-flex">
								<textarea name="product-description" id="edit-product-description" rows="7" class="mb-2 p-0 w-100 edit-product-description"><?= $product['description']; ?></textarea>
								<i class="far fa-edit"></i>
								</div>

								<!-- image -->
								<label for="customFile" class="mt-3">Upload New Image:</label>
								<div class="d-flex">
								<input type="file" class="form-control-file mt-1" id="product-image" name="product-image">
								</div>

								<!-- category -->
								<div class="input-group my-3">
									<div class="input-group-prepend">
										<label class="input-group-text" for="product-category">Product Category</label>
									</div>
									<select class="custom-select" id="product-category" name="product-category">
										<option value="" disabled selected></option>
										<?php 
										$find_query = "SELECT * FROM categories";
										$result = mysqli_query($conn, $find_query);

										while ($row = mysqli_fetch_assoc($result)) {
											?>
											<option value="<?php echo $row['id'];?>" 
												<?php if ($row['id'] == $product['category_id'])  { echo "selected"; } ?>
												>
												<?php echo $row['name'];?>
											</option>

											<?php 
										}; 
										?>

									</select>
								</div>
							<div class="row align-items-end cont-bg h-auto">
								<div class="col">
									<div class="text-center">
										<button class="btn btn-success w-100 mt-5" id="editProductBtn">Update</button>
									</div>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	}