<?php 
	
	require_once './../partials/template.php';

	// $user = !isset($_SESSION["user"]);

	// echo '<script>alert("'.$_SESSION["user"].'");</script>';

	function get_content() {?>
		<div class="container h-50">
			<div class="row mb-5 h-100 align-content-center">
				<div class="col-lg-6 col-md-8 col-sm-12 mx-auto">
					<h2 class="text-center my-5">Categories</h2>
					<div class="col-lg-8 mx-auto mb-5">
						<form action="./../controllers/process_add_category.php" method="POST">
							<div class="form-group">
								<label for="name">Category Name:</label>
								<input type="text" name="name" id="add-categ-name" class="form-control">
							</div>
							<div class="d-flex justify-content-center">
								<button id="addCategBtn" type="submit" class="btn btn-dark rounded-pill">Add Category</button>
							</div>
						</form>
					</div>
					<div class="row mb-5">
						<div class="col-auto mx-auto">
							<ul class="list-group">
								<?php
								if (!isset($_SESSION["user"]) || !isset($_SESSION["role"])) {
									header("location: home.php");
								}

								if ($_SESSION["role"] != 1) {
									header("location: home.php");
								}
									require_once './../controllers/connection.php';
									$sql_query = "SELECT * from categories";
									$result = mysqli_query($conn, $sql_query);

									while ($row = mysqli_fetch_assoc($result)) {
								?>
								<li class="list-group-item list-group-item-info text-center"> 
									<p class="categ-item">
								<?php 
									echo $row["name"];
								?>
									</p>
									<a id="" data-href="<?php echo $row['id']; ?>" class="btn btn-success rounded-pill text-white editCategBtn" data-toggle="modal" data-target="#editCategModal"><small>Edit</small></a>
									<a id="" data-href="<?php echo $row['id']; ?>" data-categ="<?php echo $row['name']; ?>" class="btn btn-danger rounded-pill delCategBtn"><small>Delete</small></a>
								</li>

								<?php 
									};
								?>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="editCategModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deleteCategLabel;">Edit Category</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form action="./../controllers/edit_categ.php" method="POST">
						<div class="modal-body">
							<input type="hidden" id="CategId" name="id" value="">
							<label for="new-name">New Category Name:</label>
							<input id="new-name" type="text" name="name">
						</div>
						<div class="modal-footer">
							<button id="goEditCateg" type="submit" class="btn btn-primary">Edit</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
	};
	?>