<?php 
	
	session_start();

	require_once './../controllers/connection.php';

	

	$get_last_transaction_query = "SELECT 
	transactions.transaction_code, 
	transactions.total, 
	users.firstname, 
	users.lastname,
	payment_modes.name as payment_name,
	statuses.name as status_name,
	transactions.id
	FROM transactions
	JOIN users ON (transactions.user_id = users.id)
	JOIN payment_modes ON (transactions.payment_mode_id = payment_modes.id)
	JOIN statuses ON (transactions.status_id = statuses.id)
	WHERE users.id = {$_SESSION['user_id']} ORDER BY transactions.id DESC LIMIT 1
	";



	$get_last_transaction = mysqli_query($conn, $get_last_transaction_query);

	while ($last_transaction = mysqli_fetch_assoc($get_last_transaction)) {
		echo "<br><br>";
		echo "Transaction Code: " . $last_transaction['transaction_code'];
		echo "<br>";
		echo "Customer: " . $last_transaction['firstname']. " " . $last_transaction['lastname'];
		echo "<br><br>";
		
		$get_last_product_transaction_query = "SELECT
		product_transactions.quantity,
		products.name,
		products.price
		FROM product_transactions
		JOIN products ON (products.id = product_transactions.product_id)
		WHERE product_transactions.transaction_id = {$last_transaction['id']}";

		$get_last_product_transaction = mysqli_query($conn, $get_last_product_transaction_query);

		while ($last_product_transaction = mysqli_fetch_assoc($get_last_product_transaction)) {
			echo "<br><br>";
			echo "Item: " . $last_product_transaction['name'];
			echo "<br>";
			echo "Quantity: " . $last_product_transaction['quantity'];
			echo "<br>";
			echo "Price: PHP ". $last_product_transaction['price'];
		}
	}