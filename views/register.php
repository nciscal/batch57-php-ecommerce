<?php
	require_once './../partials/template.php';

	function get_content() {?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 my-3">
					<h2 class="text-center">Registration Form</h2>
					<!-- input form -->
					<!-- <form action="./../controllers/process_register.php" method="POST" id="registration-form" class="mt-5"> -->
					<form action="#" method="POST" id="registration-form" class="mt-5">
						<div class="form-group">
							<label for="firstname">First Name:</label><span>OK</span>
							<input type="text" name="firstname" id="firstname" class="form-control">
						</div>
						<div class="form-group">
							<label for="lastname">Last Name:</label><span>OK</span>
							<input type="text" name="lastname" id="lastname" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email:</label><span>OK</span>
							<input type="email" name="email" id="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="password">Password:</label><span>OK</span>
							<input type="password" name="password" id="password" class="form-control">
						</div>
						<div class="form-group">
							<label for="confirm_password">Confirm Password:</label><span>OK</span>
							<input type="password" name="confirm_password" id="confirm_password" class="form-control">
						</div>
						<div class="col-auto p-0 my-4">
						<button id="regBtn" type="submit" class="btn btn-dark w-100">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php 
	};
	?>