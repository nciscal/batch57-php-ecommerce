<?php

	require_once './../partials/template.php';

	function get_content() {?>
		<?php  
			require_once './../controllers/connection.php';
			if (!isset($_SESSION["user"])) {
				header("location: home.php");
			}

			if ($_SESSION["role"] != 1) {
				header("location: home.php");
			}
		?>
		<div class="container">
			<div class="row my-5">
				<div class="col-xl-6 col-lg-8 col-md-8 col-sm-12 mx-auto">
					<h2 class="text-center mb-5">Add Product Form</h2>
					<form id="addProductForm" action="./../controllers/product_add.php" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label for="product-name">Product Name:</label>
							<input type="text" name="product-name" id="product-name" class="form-control">
						</div>
						<div class="form-group">
							<label for="product-price">Product Price:</label>
							<input type="text" name="product-price" id="product-price" class="form-control">
						</div>
						<div class="form-group">
							<!-- <label for="product-image">Product Image:</label>
							<input type="file" name="product-image" id="product-image" class="form-control"> -->
							<label for="customFile" class="">Product Image:</label>
						
							<input type="file" class="form-control-file" id="customFile" name="product-image">
		
							<!-- <div class="custom-file overflow-hidden rounded-pill">
								<input id="customFile" type="file" class="form-control-file rounded-pill" name="product-image">
								<label for="customFile" class="custom-file-label rounded-pill">Choose Image</label>
							</div> -->
						</div>
						<div class="form-group">
							<label for="product-description">Product Description:</label>
							<textarea name="product-description" id="product-description" rows="5" class="form-control"> </textarea>
						</div>
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<label class="input-group-text" for="product-category">Product Category</label>
							</div>
							<select class="custom-select" id="product-category" name="product-category">
								<?php 
									$find_query = "SELECT * FROM categories";
									$result = mysqli_query($conn, $find_query);

									while ($row = mysqli_fetch_assoc($result)) {
								?>
								<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>

								<?php 
								}; 
								?>
							</select>
						</div>
						<div class="text-center">
							<button class="btn btn-warning" id="addProductBtn">Add New Product</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
	};
	?>