<?php 
	require_once './../partials/template.php';

	function get_content() {

		//connect to database
		require_once './../controllers/connection.php';

		$get_transactions = "SELECT 
			transactions.transaction_code, 
			transactions.total, 
			users.firstname, 
			users.lastname,
			payment_modes.name as payment_name,
			statuses.name as status_name,
			transactions.id
		FROM transactions
		JOIN users ON (transactions.user_id = users.id)
		JOIN payment_modes ON (transactions.payment_mode_id = payment_modes.id)
		JOIN statuses ON (transactions.status_id = statuses.id)
		WHERE users.id = {$_SESSION['user_id']}
		";

		$result = mysqli_query($conn, $get_transactions);
		?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-12 my-3">
					<!-- start of transaction table details-->
					<h2 class="text-center mt-5">Transaction Details</h2>
					<div class="table-responsive my-5">
						<?php 
								while($transaction = mysqli_fetch_assoc($result)) {
							?>
						<table class="table table-hover table-info">
							<tr>
								<th>Transaction Code:</th>
								<td><?= $transaction['transaction_code']?></td>
							</tr>
							<!-- customer -->
							<tr>
								<th>Customer Name:</th>
								<td><?php echo $transaction['firstname'] . " " . $transaction['lastname']; ?></td>
							</tr>
							<!-- payment mode -->
							<tr>
								<th>Payment Mode:</th>
								<td><?php echo $transaction['payment_name']; ?></td>
							</tr>
							<!-- transaction status -->
							<tr>
								<th>Status:</th>
								<td><?php echo $transaction['status_name'];?></td>
							</tr>
						</table>
						<!-- purchased items -->
						<table class="table table-hover table-info">
							<tbody>
								<tr>
									<th> Product Name </th>
									<th> Price per Product </th>
									<th> Quantity </th>
									<th> Subtotal </th>
								</tr>
									<?php
									$total = 0;
									$get_product_transactions = "
										SELECT
											product_transactions.quantity,
											products.name,
											products.price
										FROM product_transactions
										JOIN products ON (products.id = product_transactions.product_id)
										WHERE product_transactions.transaction_id = {$transaction['id']}";
									
									$result_products = mysqli_query($conn, $get_product_transactions);
									while ($product = mysqli_fetch_assoc($result_products)) {

										$price = number_format($product['price'],2);

									?>
								<tr>
									<td><?php echo $product['name']; ?></td>
									<td>&#8369; <?php echo $price; ?></td>
									<td><?php echo $product['quantity']; ?></td>
									<td>&#8369;
										<?php 
											$subtotal = $product['price'] * $product['quantity'];
											$total += $subtotal;
											echo number_format($subtotal,2);
										?>
									</td>
								</tr>
								<?php };?>
							</tbody>
							<tfoot>
								<tr class="bg-info">
									<th colspan="3">Total: </th>
									<th>&#8369; <?php echo number_format($total,2); ?></th>
								</tr>
							</tfoot>
						</table>
					<?php };?>
					</div>
					<!-- end of transaction table details -->
				</div>
			</div>
		</div>
	<?php
	};
?>