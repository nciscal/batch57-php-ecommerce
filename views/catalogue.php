<?php

    require_once './../partials/template.php';


function get_content() {?>

	<?php require_once "./../controllers/connection.php";

	$isSetUser = false;
	$isAdmin = false;

	if (isset($_SESSION['user']) && isset($_SESSION['role'])) {
		$isSetUser = true;
		if ($_SESSION['role'] == 1) {
			$isAdmin = true;
		}
	} else {
		$isSetUser = false;
		header("location: login.php");
	}

	?>

		<div class="container">
			<div class="row">
				<div class="col-lg-12 my-5">
					<h2 class="text-center">Shop</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-auto mb-3">
					<!-- filter of categories -->

					<!-- show all -->
					<div class="dropdown ml-2">
						<button class="btn btn-outline-dark dropdown-toggle btn-hover" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Filter
						</button>
						<div id="dropdown-menu" class="dropdown-menu btn-hover" aria-labelledby="dropdownMenu2">

							<a class="dropdown-item btn-hover" type="button" href="./catalogue.php">Show All</a>

					<!-- show each category -->

						<?php
						$category_query = "SELECT * FROM categories";
						$categories = mysqli_query( $conn, $category_query );
						foreach ( $categories as $category ) {
							?>
							<a href="./catalogue.php?page=1&category_id=<?=$category['id'];?>" class="dropdown-item btn-hover" type="button"><?=$category['name'];?></a>

							<?php
						}
						;?>
						</div>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-100">
					<!-- pagination -->
					<?php

					if (isset($_GET['page']) && isset($_GET['category_id']) ) {
						$is_all = true;
						$page = $_GET['page'];
						$category_id = $_GET['category_id'];
						$total_pages_sql = "SELECT COUNT(*) FROM products WHERE category_id =" . $_GET['category_id'];
					} else if (isset($_GET['page'])) {
						$page = $_GET['page'];
						$is_all = false;
						$total_pages_sql = "SELECT COUNT(*) FROM products";
					} else {
						$page = 1;
						$is_all = false;
						$total_pages_sql = "SELECT COUNT(*) FROM products";
					}
					//set how many items per page
					$no_of_items_per_page = 9;
					$offset = ($page-1) * $no_of_items_per_page;

					//get total number of pages
					
					$result = mysqli_query($conn,$total_pages_sql);
					$total_rows = mysqli_fetch_array($result)[0];
					$total_pages = ceil($total_rows / $no_of_items_per_page);
					
					?>
					<nav aria-label="page navigation">
	        			<ul class="pagination justify-content-center">
	        				<li class="page-item <?php if($page <= 1){ echo 'disabled'; } ?>">
	        					<a class="page-link" href=
		        					'<?php if (!$is_all) {
		        						echo "./catalogue.php?page=1";
		        					} else {
		        						echo "./catalogue.php?page=1&category_id={$category_id}"; 
		        					} ?>'

	        						tabindex="-1">&laquo;
	        					</a>
	        				</li>
	        				<?php
	        					for ($pg=1;$pg<=$total_pages;$pg++) {?>
									<li class="page-item"><a class="page-link" href='<?php if ($is_all == true) {
											echo "./catalogue.php?page={$pg}&category_id={$category_id}";
										} else {
											 echo "./catalogue.php?page={$pg}";
										} ?>'
									><?=$pg?></a></li>
	        				<?php	
	        					};
	        				?>
	        				<li class="page-item <?php if($page >= $total_pages){ echo 'disabled'; } ?>">
	        					<a class="page-link" href="?page=<?= $total_pages; ?>">&raquo;</a>
	        				</li>
	        			</ul>
	        		</nav>
        		</div>
			</div>
			<div class="row mb-5">
		<?php
		$sql = "SELECT * FROM products";
		$limit_query = " LIMIT $offset, $no_of_items_per_page";

        $sql_query = "SELECT * FROM products LIMIT $offset, $no_of_items_per_page";

            if ( isset( $_GET['category_id'] ) ) {
            	$condition_query = " WHERE category_id =" . $_GET['category_id'];
                $sql_query =  $sql.$condition_query.$limit_query;
            }
            
            $products = mysqli_query( $conn, $sql_query );

        	foreach ( $products as $product ) {?>

				<!-- card structure -->

				<div class="col-lg-4 col-md-6 col-sm-12 px-4 my-3">
					<div class="card h-100">
						<img src="./../assets/images/<?=$product['image']?>" alt="product image" class="card-img-top shop-item-img">
						<div class="card-body">

							<h4 class="card-title"><?=$product['name']?></h4>
							<p class="card-text">
								<?=$product['description'];?>
								<br>
								<?= "₱".number_format($product['price'], 2);?>
							</p>

						</div>
						<div class="card-footer">
							<!-- quantity to buy -->
							<div class="d-flex justify-content-center align-items-center">
								<button type="button" class="shop-minus disabled" disabled>-</button>
								<input type="number" class="form-control shop-item-qty" value="1">
								<button type="button" class="shop-add">+</button>
							</div>
							<div class="col-auto text-center mt-3">
								<button class="btn btn-dark addToCart" data-id="<?=$product['id'];?>">Add to Cart</button>
							</div>

							<!-- delete & update product -->
							<?php
								if ($isSetUser) {
									if ($isAdmin) {?>

										<div class="col-auto text-center mt-3">
											<a href="./edit_product.php?id=<?= $product['id']?>" class="btn btn-dark my-1">Edit
											</a>
											<a data-href="../controllers/product_delete.php?id=" data-id="<?php echo $product['id']?>" class="btn btn-dark text-white my-1 deleteProductBtn">Delete
											</a>
										</div>
							<?php	}
								}
							?>
						</div>
					</div>
				</div>
		<?php
            }
            ?>
			</div>
		</div>	
<?php
};
?>