<?php
	
	require_once '../partials/template.php';


	function get_content() {?>

		<div class="col-lg-12 p-0 min-vh-100 hero-container">
			<div class="jumbotron text-white">
				<h1 class="display-4">Gift Ideas for Mother's Day,</h1>
				<p class="lead">Shop now and receive a complimentary gift from us.</p>
				<a class="btn btn-dark btn-lg" href="./catalogue.php" role="button">Shop Now</a>
			</div>
			
		</div>
<?php
};
?>