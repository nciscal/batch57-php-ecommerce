<?php
	require_once '../partials/template.php';

	function get_content() {
		if(isset($_SESSION["user"]))
		{
			header("Location: ./../views/home.php");
		}
		?>
		<div class="container mb-5">
			<div class="row align-items-center">
				<div class="col-xl-4 col-lg-6 col-md-8 col-sm-12 mx-auto my-3">
					<h2 class="text-center mb-3">Login</h2>
					<!-- input form -->
					<form id="login-form" action="./../controllers/authenticate.php" method="POST">
						<div class="form-group">
							<label for="email">Email:</label><span>OK</span>
							<input type="email" name="email" id="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="password">Password:</label><span>OK</span>
							<input type="password" name="password" id="password" class="form-control">
						</div>
						<div class="col-auto p-0 mt-4">
							<button type="submit" class="btn btn-dark w-100">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
<?php	
	};
?>